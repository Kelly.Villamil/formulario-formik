import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';


class Creador extends React.Component{

    enviarformulario(valores, acciones){
        console.log(valores);//los valores que envian en el formulario
        
        fetch('http://localhost:3000/insertar',    
            {
                method:'POST',
                headers: {  'Content-type':'application/json',
                            'cors':'Access-Control-Allow-Origin' },
                body: JSON.stringify({
                    user:{
                        id: valores.id,
                        nombre: valores.nombre
                    }
                    
                })
            },
        );
                 
    };
        
    render(){
        let elemento= <Formik 
            initialValues ={
                {
                    id:'',
                    nombre:''
                }
            }
            onSubmit={this.enviarformulario}

            validationSchema={
                Yup.object().shape(
                    {
                        id: Yup.number().typeError('Debe ser un numero').required('Es obligatorio'),
                        nombre: Yup.string().required('Campo es obligatorio')
                    }
                )
            }
            >
                <Container className="p-3">
                    <Form>
                        <div className="form-group">
                            <label htmlFor="id">ID </label>
                            <Field name="id" type="text" className='form-control'></Field>
                            <ErrorMessage name='id' className='invalid-feedback'></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre </label>
                            <Field name="nombre" type="text" className='form-control'></Field>
                            <ErrorMessage name='nombre' className='invalid-feedback'></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <Button type="submit" className="btn btn-primary m-2">Enviar</Button>
                            <Button type="reset" className="btn btn-secondary m-2">Cancelar</Button>

                        </div>
                    </Form>
                </Container>
            </Formik>;
            return elemento;
    };
}

export default Creador;